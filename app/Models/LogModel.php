<?php

namespace App\Models;

use CodeIgniter\Model;

class LogModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'logs';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['nama_petugas', 'deskripsi', 'created_at'];


    function search_and_display($query = null, $start = 0, $length = 0)
    {
        $builder = $this->table('logs');

        if ($query) {
            $arr_query = explode(" ", $query);
            for ($x = 0; $x < count($arr_query); $x++) {
                $builder = $builder->orLike('nama_petugas', $arr_query[$x]);
                $builder = $builder->orLike('deskripsi', $arr_query[$x]);
                $builder = $builder->orLike('created_at', $arr_query[$x]);
            }
        }

        if ($start != 0 or $length != 0) {
            $builder = $builder->limit($length, $start);
        }

        return $builder->orderBy('created_at', 'desc')->get()->getResult();
    }
}
