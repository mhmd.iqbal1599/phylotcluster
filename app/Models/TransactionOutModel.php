<?php

namespace App\Models;

use CodeIgniter\Model;

class TransactionOutModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'transaksi_keluar';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['rfid_keluar', 'tglkeluar', 'fotokeluar'];


    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    function search_and_display($query = null, $start = 0, $length = 0)
    {
        $builder = $this->table('transaksi_keluar');

        if ($query) {
            $arr_query = explode(" ", $query);
            for ($x = 0; $x < count($arr_query); $x++) {
                $builder = $builder->orLike('rfid_keluar', $arr_query[$x]);
                $builder = $builder->orLike('tglkeluar', $arr_query[$x]);
            }
        }

        if ($start != 0 or $length != 0) {
            $builder = $builder->limit($length, $start);
        }

        return $builder->orderBy('tglkeluar', 'desc')->get()->getResult();
    }
}
