<?php

namespace App\Models;

use CodeIgniter\Model;

class TransactionModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'transaksi';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [
        'rfid',
        'tglmasuk',
        'fotomasuk',
        'idpetugas',
        'keperluan',
        'plat'
    ];

    function search_and_display($query = null, $start = 0, $length = 0)
    {
        $builder = $this->table('transaksi');

        if ($query) {
            $arr_query = explode(" ", $query);
            for ($x = 0; $x < count($arr_query); $x++) {
                $builder = $builder->orLike('rfid_masuk', $arr_query[$x]);
                $builder = $builder->orLike('tglmasuk', $arr_query[$x]);
                $builder = $builder->orLike('keperluan', $arr_query[$x]);
                $builder = $builder->orLike('plat', $arr_query[$x]);
            }
        }

        if ($start != 0 or $length != 0) {
            $builder = $builder->limit($length, $start);
        }

        return $builder->orderBy('tglmasuk', 'desc')->get()->getResult();
    }
}
