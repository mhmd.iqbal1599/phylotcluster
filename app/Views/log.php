<body class="g-sidenav-show bg-gray-100">
  <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-radius-xl border-0 my-3 fixed-start ms-3" id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href="<?= base_url('dashboard'); ?>">
        <img src="<?= base_url(); ?>/assets/images/logo.svg" class="navbar-brand-img h-100" alt="main_logo">
        <span class="ms-1 font-weight-bold text-info text-gradient">PhylotCluster</span>
      </a>
    </div>
    <hr class="horizontal dark mt-0">
    <div class="collapse navbar-collapse max-height-vh-100 h-50 w-auto" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <?php

        if (session()->level == 'Petugas') :
        ?>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('dashboard'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-spaceship text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('stream'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-button-play text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Streaming</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('master'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-chart-bar-32 text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Master</span>
            </a>
          </li>
        <?php
        endif;
        ?>
        <?php
        if (session()->level == 'Owner') :
        ?>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('dashboard'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-spaceship text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('stream'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-button-play text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Streaming</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('master'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-chart-bar-32 text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Master</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('report'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-archive-2 text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Report</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="<?= base_url('log'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-books text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Log</span>
            </a>
          </li>
        <?php
        endif;
        ?>
        <li class="nav-item">
          <a class="nav-link" href="<?= base_url('akun') . '/' . session()->username; ?>">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-single-02 text-dark"></i>
            </div>
            <span class="nav-link-text ms-1">Profile</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="<?= base_url('auth/logout'); ?>">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-button-power text-dark"></i>
            </div>
            <span class="nav-link-text ms-1">Logout</span>
          </a>
        </li>
      </ul>
    </div>
    <div class="sidenav-footer mx-3 my-3">
      <div class="card card-background shadow-none card-background-mask-secondary" id="sidenavCard">
        <div class="full-background" style="background-image: url('<?= base_url(); ?>/assets/template/img/curved-images/white-curved.jpeg')"></div>
        <div class="card-body text-start p-3 w-100">
          <div class="icon icon-shape icon-sm bg-white shadow text-center mb-3 d-flex align-items-center justify-content-center border-radius-md">
            <i class="fa fa-whatsapp text-dark text-gradient text-lg top-0" aria-hidden="true" id="sidenavCardIcon"></i>
          </div>
          <div class="docs-info">
            <h6 class="text-white up mb-0">Call Support</h6>
            <p class="text-xs font-weight-bold">Please contact us in WhatsApp.</p>
            <a href="https://wa.me/6282287778685" target="_blank" class="btn btn-white w-100 mb-0">Click here</a>
          </div>
        </div>
      </div>
    </div>
  </aside>
  <main class="main-content position-relative max-height-vh-100 h-100">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none" id="navbarBlur" navbar-scroll="true">
      <div class="container-fluid py-1 px-3">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark" aria-current="page">Log</li>
          </ol>
        </nav>
        <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
          <div class="ms-md-auto pe-md-3 d-flex align-items-center">
          </div>
          <ul class="navbar-nav  justify-content-end">
            <li class="nav-item d-xl-none d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </a>
            </li>
            <li class="nav-item d-flex align-items-center ps-4">
              <a href="<?= base_url('akun') . '/' . session()->username ?>" class="btn btn-tooltip mb-0 bg-gradient-primary text-white px-3" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Profile" data-container="body" data-animation="true">
                <i class="fa fa-user me-sm-1"></i>
                <span class="d-sm-inline d-none">Akun</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- End Navbar -->

    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              <h6>Logs</h6>
            </div>
            <div class="card-body px-0 pt-0 pb-2 px-4">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0" id="logs">
                  <thead>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Nama Petugas</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Deskripsi</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Waktu</th>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      <script>
        $(document).ready(function() {
          $('#logs').DataTable({
            'processing': true,
            'serverSide': true,
            "pagingType": "full_numbers",
            'serverMethod': 'post',
            'ajax': "<?= base_url('log/logs_ajax'); ?>",
            'columns': [{
                'data': 'nama_petugas'
              },
              {
                'data': 'deskripsi'
              },
              {
                'data': 'created_at'
              },
            ]
          });
        });
      </script>