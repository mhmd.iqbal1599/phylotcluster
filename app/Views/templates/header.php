<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url(); ?>/assets/template/img/apple-icon.png">
  <link rel="icon" type="image/png" href="<?= base_url(); ?>/assets/images/logo.svg">
  <title>
    PhylotCluster Dashboard
  </title>
  <!-- custom css -->
  <link href="<?= base_url(); ?>/assets/css/styles.css" rel="stylesheet" />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
  <!-- Nucleo Icons -->
  <link href="<?= base_url(); ?>/assets/template/css/nucleo-icons.css" rel="stylesheet" />
  <link href="<?= base_url(); ?>/assets/template/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <link href="<?= base_url(); ?>/assets/template/css/nucleo-svg.css" rel="stylesheet" />
  <!-- CSS Files -->

  <script src="<?= base_url('assets/plugin/datatables'); ?>/jQuery-3.6.0/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

  <link id="pagestyle" href="<?= base_url(); ?>/assets/template/css/soft-ui-dashboard.css?v=1.0.3" rel="stylesheet" />

</head>