<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url(); ?>/assets/template/img/apple-icon.png">
  <link rel="icon" type="image/png" href="<?= base_url(); ?>/assets/images/logo.svg">
  <title>
    PhylotCluster Dashboard
  </title>
  <!-- custom css -->
  <link href="<?= base_url(); ?>/assets/css/styles.css" rel="stylesheet" />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
  <!-- Nucleo Icons -->
  <link href="<?= base_url(); ?>/assets/template/css/nucleo-icons.css" rel="stylesheet" />
  <link href="<?= base_url(); ?>/assets/template/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <link href="<?= base_url(); ?>/assets/template/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Data Tables Files -->
  <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugin/datatables'); ?>/DataTables-1.11.3/css/dataTables.bootstrap5.min.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugin/datatables'); ?>/Buttons-2.1.1/css/buttons.bootstrap5.min.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugin/datatables'); ?>/DateTime-1.1.1/css/dataTables.dateTime.min.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugin/datatables'); ?>/FixedHeader-3.2.1/css/fixedHeader.bootstrap5.min.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugin/datatables'); ?>/Responsive-2.2.9/css/responsive.bootstrap5.min.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugin/datatables'); ?>/Scroller-2.0.5/css/scroller.bootstrap5.min.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugin/datatables'); ?>/SearchPanes-1.4.0/css/searchPanes.bootstrap5.min.css" />

  <script type="text/javascript" src="<?= base_url('assets/plugin/datatables'); ?>/jQuery-3.6.0/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <script type="text/javascript" src="<?= base_url('assets/plugin/datatables'); ?>/JSZip-2.5.0/jszip.min.js"></script>
  <script type="text/javascript" src="<?= base_url('assets/plugin/datatables'); ?>/pdfmake-0.1.36/pdfmake.min.js"></script>
  <script type="text/javascript" src="<?= base_url('assets/plugin/datatables'); ?>/pdfmake-0.1.36/vfs_fonts.js"></script>
  <script type="text/javascript" src="<?= base_url('assets/plugin/datatables'); ?>/DataTables-1.11.3/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="<?= base_url('assets/plugin/datatables'); ?>/DataTables-1.11.3/js/dataTables.bootstrap5.min.js"></script>
  <script type="text/javascript" src="<?= base_url('assets/plugin/datatables'); ?>/Buttons-2.1.1/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="<?= base_url('assets/plugin/datatables'); ?>/Buttons-2.1.1/js/buttons.bootstrap5.min.js"></script>
  <script type="text/javascript" src="<?= base_url('assets/plugin/datatables'); ?>/Buttons-2.1.1/js/buttons.colVis.min.js"></script>
  <script type="text/javascript" src="<?= base_url('assets/plugin/datatables'); ?>/Buttons-2.1.1/js/buttons.html5.min.js"></script>
  <script type="text/javascript" src="<?= base_url('assets/plugin/datatables'); ?>/Buttons-2.1.1/js/buttons.print.min.js"></script>
  <script type="text/javascript" src="<?= base_url('assets/plugin/datatables'); ?>/DateTime-1.1.1/js/dataTables.dateTime.min.js"></script>
  <script type="text/javascript" src="<?= base_url('assets/plugin/datatables'); ?>/FixedHeader-3.2.1/js/dataTables.fixedHeader.min.js"></script>
  <script type="text/javascript" src="<?= base_url('assets/plugin/datatables'); ?>/Responsive-2.2.9/js/dataTables.responsive.min.js"></script>
  <script type="text/javascript" src="<?= base_url('assets/plugin/datatables'); ?>/Responsive-2.2.9/js/responsive.bootstrap5.js"></script>
  <script type="text/javascript" src="<?= base_url('assets/plugin/datatables'); ?>/Scroller-2.0.5/js/dataTables.scroller.min.js"></script>
  <script type="text/javascript" src="<?= base_url('assets/plugin/datatables'); ?>/SearchPanes-1.4.0/js/dataTables.searchPanes.min.js"></script>
  <script type="text/javascript" src="<?= base_url('assets/plugin/datatables'); ?>/SearchPanes-1.4.0/js/searchPanes.bootstrap5.min.js"></script>
  <link id="pagestyle" href="<?= base_url(); ?>/assets/template/css/soft-ui-dashboard.css?v=1.0.3" rel="stylesheet" />

</head>