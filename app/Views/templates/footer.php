<footer class="footer pt-3">
  <div class="container-fluid">
    <div class="row align-items-center justify-content-lg-between">
      <div class="col-lg-12 mb-lg-0 mb-4">
        <div class="copyright text-center text-sm text-muted text-lg-start">
          © <script>
            document.write(new Date().getFullYear())
          </script>.
          Development by
          <a href="#" class="font-weight-bold" target="_blank">Phylot</a>. Support WA : <a href="https://wa.me/6282287778685" class="font-weight-bold">082287778685</a>.
        </div>
      </div>
    </div>
  </div>
</footer>
</div>
</main>

<!--   Core JS Files   -->
<script src="<?= base_url(); ?>/assets/template/js/core/popper.min.js"></script>
<script src="<?= base_url(); ?>/assets/template/js/core/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>/assets/template/js/plugins/perfect-scrollbar.min.js"></script>
<script src="<?= base_url(); ?>/assets/template/js/plugins/smooth-scrollbar.min.js"></script>

<script>
  var win = navigator.platform.indexOf('Win') > -1;
  if (win && document.querySelector('#sidenav-scrollbar')) {
    var options = {
      damping: '0.5'
    }
    Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
  }
</script>
<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?= base_url(); ?>/assets/template/js/soft-ui-dashboard.min.js?v=1.0.3"></script>
</body>

</html>