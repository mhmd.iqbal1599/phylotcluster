<body class="g-sidenav-show bg-gray-100">
  <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-radius-xl border-0 my-3 fixed-start ms-3" id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href="<?= base_url('dashboard'); ?>">
        <img src="<?= base_url(); ?>/assets/images/logo.svg" class="navbar-brand-img h-100" alt="main_logo">
        <span class="ms-1 font-weight-bold text-info text-gradient">PhylotCluster</span>
      </a>
    </div>
    <hr class="horizontal dark mt-0">
    <div class="collapse navbar-collapse max-height-vh-100 h-75 w-auto" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <?php

        if (session()->level == 'Petugas') :
        ?>
          <li class="nav-item">
            <a class="nav-link active" href="<?= base_url('dashboard'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-spaceship text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('stream'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-button-play text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Streaming</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link  " href="<?= base_url('master'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-chart-bar-32 text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Master</span>
            </a>
          </li>
        <?php
        endif;
        ?>
        <?php
        if (session()->level == 'Owner') :
        ?>
          <li class="nav-item">
            <a class="nav-link active" href="<?= base_url('dashboard'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-spaceship text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('stream'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-button-play text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Streaming</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link  " href="<?= base_url('master'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-chart-bar-32 text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Master</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link  " href="<?= base_url('report'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-archive-2 text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Report</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link  " href="<?= base_url('log'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-books text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Log</span>
            </a>
          </li>
        <?php
        endif;
        ?>
        <li class="nav-item">
          <a class="nav-link" href="<?= base_url('akun') . '/' . session()->username; ?>">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-single-02 text-dark"></i>
            </div>
            <span class="nav-link-text ms-1">Profile</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link  " href="<?= base_url('auth/logout'); ?>">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-button-power text-dark"></i>
            </div>
            <span class="nav-link-text ms-1">Logout</span>
          </a>
        </li>
      </ul>
    </div>
    <div class="sidenav-footer mx-3 my-3">
      <div class="card card-background shadow-none card-background-mask-secondary" id="sidenavCard">
        <div class="full-background" style="background-image: url('<?= base_url(); ?>/assets/template/img/curved-images/white-curved.jpeg')"></div>
        <div class="card-body text-start p-3 w-100">
          <div class="icon icon-shape icon-sm bg-white shadow text-center mb-3 d-flex align-items-center justify-content-center border-radius-md">
            <i class="fa fa-whatsapp text-dark text-gradient text-lg top-0" aria-hidden="true" id="sidenavCardIcon"></i>
          </div>
          <div class="docs-info">
            <h6 class="text-white up mb-0">Call Support</h6>
            <p class="text-xs font-weight-bold">Please contact us in WhatsApp.</p>
            <a href="https://wa.me/6282287778685" target="_blank" class="btn btn-white w-100 mb-0">Click here</a>
          </div>
        </div>
      </div>
    </div>
  </aside>
  <main class="main-content position-relative max-height-vh-100 h-100">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none" id="navbarBlur" navbar-scroll="true">
      <div class="container-fluid py-1 px-3">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark" aria-current="page">Dashboard</li>
          </ol>
        </nav>
        <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
          <div class="ms-md-auto pe-md-3 d-flex align-items-center">
          </div>
          <ul class="navbar-nav  justify-content-end">
            <li class="nav-item d-xl-none d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </a>
            </li>
            <li class="nav-item d-flex align-items-center ps-4">
              <a href="<?= base_url('akun') . '/' . $_SESSION['username'] ?>" class="btn btn-tooltip mb-0 bg-gradient-primary text-white px-3" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Profile" data-container="body" data-animation="true">
                <i class="fa fa-user me-sm-1"></i>
                <span class="d-sm-inline d-none">Akun</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- End Navbar -->

    <?php if (!empty(session()->getFlashdata('success'))) : ?>
      <div class="alert alert-success mx-4 my-2 text-white" role="alert"><?= session()->getFlashdata('success'); ?> Selamat datang kembali, <strong><?= $_SESSION['nama']; ?></strong></div>
    <?php endif; ?>

    <?php if (!empty(session()->getFlashdata('update_success'))) : ?>
      <div class="alert alert-success mx-4 my-2 text-white" role="alert"><?= session()->getFlashdata('update_success'); ?></div>
    <?php endif; ?>

    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              <h6>Tamu Hari Ini</h6>
            </div>
            <div class="card-body px-0 pt-0 pb-2 px-4">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0" id="transaksi">
                  <thead>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Aksi</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Foto Masuk</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Waktu Masuk</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Plat</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Keperluan</th>
                  </thead>
                  <tbody>
                    <?php
                    foreach ($transaction as $t) :
                    ?>
                      <tr>
                        <td>
                          <div class="col-md-4">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn bg-gradient-success btn-block mb-3" data-bs-toggle="modal" data-bs-target="#inputDataModal">
                              Input Data
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="inputDataModal" tabindex="-1" role="dialog" aria-labelledby="inputDataModalTitle" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Input Data Tamu</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">×</span>
                                    </button>
                                  </div>
                                  <form action="<?= base_url('dashboard/update') . '/' . $t['id']; ?>" method="POST">
                                    <?= csrf_field() ?>
                                    <div class="modal-body">
                                      <input type="hidden" class="form-control" name="id" value="<?= $t['id']; ?>">
                                      <div class="form-group">
                                        <label for="plat" class="col-form-label">Plat</label>
                                        <input type="text" class="form-control" id="plat" name="plat" placeholder="BG 1234 ABC">
                                      </div>
                                      <div class="form-group">
                                        <label for="keperluan" class="col-form-label">Keperluan</label>
                                        <textarea class="form-control" name="keperluan" id="keperluan" placeholder="Menemui RT"></textarea>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-link ml-auto text-danger" data-bs-dismiss="modal">Batal</button>
                                      <button type="submit" name="submit" class="btn bg-gradient-primary">Simpan</button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>
                        </td>
                        <td>
                          <img src="<?= base_url('assets/images') . '/' . $t['fotomasuk']; ?>" alt="Foto Masuk" class="img-thumbnail" style="width: 130px; cursor: pointer;" data-bs-toggle="modal" data-bs-target="#imageModal<?= $t['id']; ?>">
                          <div class="modal fade" id="imageModal<?= $t['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="imageModal<?= $t['id']; ?>" aria-hidden="true">
                            <div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h6 class="modal-title" id="modal-title-notification">Detail Foto</h6>
                                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                  <div class="py-3 text-center">
                                    <img src="<?= base_url('assets/images') . '/' . $t['fotomasuk']; ?>" alt="Detail Foto" class="img-thumbnail">
                                  </div>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary text-white ml-auto" data-bs-dismiss="modal">Tutup</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </td>
                        <td>
                          <p class="text-md font-weight-bold mb-0"><?= $t['tglmasuk']; ?></p>
                        </td>
                        <td>
                          <p class="text-md font-weight-bold mb-0"><?= ($t['plat']) ? $t['plat'] : 'Belum diinput'; ?></p>
                        </td>
                        <td>
                          <p class="text-md font-weight-bold mb-0"><?= ($t['keperluan']) ? $t['keperluan'] : 'Belum diinput'; ?></p>
                        </td>
                      </tr>
                    <?php
                    endforeach;
                    ?>
                  </tbody>

                </table>
                <p class="text-danger text-xs">* Klik pada foto untuk memperbesar foto.</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-xl-4 col-md-6 mb-4">
          <div class="card">
            <div class="card-body p-3">
              <div class="row">
                <div class="col-8">
                  <div class="numbers">
                    <p class="text-xs mb-0 text-capitalize font-weight-bold">Total Tamu Harian</p>
                    <h5 class="font-weight-bolder mb-0">
                      <?= $c_tamu_harian; ?>
                    </h5>
                  </div>
                </div>
                <div class="col-4 text-end">
                  <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                    <i class="fa fa-chart-line text-lg opacity-10" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-md-6 mb-4">
          <div class="card">
            <div class="card-body p-3">
              <div class="row">
                <div class="col-8">
                  <div class="numbers">
                    <p class="text-xs mb-0 text-capitalize font-weight-bold">Total Tamu Bulanan</p>
                    <h5 class="font-weight-bolder mb-0">
                      <?= $c_tamu_bulanan; ?>
                    </h5>
                  </div>
                </div>
                <div class="col-4 text-end">
                  <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                    <i class="fa fa-chart-line text-lg opacity-10" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-md-6 mb-4">
          <div class="card">
            <div class="card-body p-3">
              <div class="row">
                <div class="col-8">
                  <div class="numbers">
                    <p class="text-xs mb-0 text-capitalize font-weight-bold">Statik Member Harian</p>
                    <h5 class="font-weight-bolder mb-0">
                      <?= $c_member_harian; ?>
                    </h5>
                  </div>
                </div>
                <div class="col-4 text-end">
                  <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                    <i class="fa fa-chart-line text-lg opacity-10" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-md-6 mb-4">
          <div class="card">
            <div class="card-body p-3">
              <div class="row">
                <div class="col-8">
                  <div class="numbers">
                    <p class="text-xs mb-0 text-capitalize font-weight-bold">Statik Member Bulanan</p>
                    <h5 class="font-weight-bolder mb-0">
                      <?= $c_member_bulanan; ?>
                    </h5>
                  </div>
                </div>
                <div class="col-4 text-end">
                  <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                    <i class="fa fa-chart-line text-lg opacity-10" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-md-6 mb-4">
          <div class="card">
            <div class="card-body p-3">
              <div class="row">
                <div class="col-8">
                  <div class="numbers">
                    <p class="text-xs mb-0 text-capitalize font-weight-bold">Total Pengunjung Harian</p>
                    <h5 class="font-weight-bolder mb-0">
                      <?= $c_pengunjung_harian; ?>
                    </h5>
                  </div>
                </div>
                <div class="col-4 text-end">
                  <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                    <i class="fa fa-chart-line text-lg opacity-10" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-md-6 mb-4">
          <div class="card">
            <div class="card-body p-3">
              <div class="row">
                <div class="col-8">
                  <div class="numbers">
                    <p class="text-xs mb-0 text-capitalize font-weight-bold">Total Pengunjung Bulanan</p>
                    <h5 class="font-weight-bolder mb-0">
                      <?= $c_pengunjung_bulanan; ?>
                    </h5>
                  </div>
                </div>
                <div class="col-4 text-end">
                  <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                    <i class="fa fa-chart-line text-lg opacity-10" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-6 col-md-6 mb-4">
          <div class="card">
            <div class="card-body p-3">
              <div class="row">
                <div class="col-8">
                  <div class="numbers">
                    <p class="text-xs mb-0 text-capitalize font-weight-bold">Total Pengunjung Menggunakan Sistem</p>
                    <h5 class="font-weight-bolder mb-0">
                      <?= $c_pengunjung_gunakan_sistem; ?>
                    </h5>
                  </div>
                </div>
                <div class="col-4 text-end">
                  <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                    <i class="fa fa-user-clock text-lg opacity-10" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-6 col-md-6 mb-4">
          <div class="card">
            <div class="card-body p-3">
              <div class="row">
                <div class="col-8">
                  <div class="numbers">
                    <!-- user di master -->
                    <p class="text-xs mb-0 text-capitalize font-weight-bold">Total Pengguna Sistem</p>
                    <h5 class="font-weight-bolder mb-0">
                      <?= $c_users; ?>
                    </h5>
                  </div>
                </div>
                <div class="col-4 text-end">
                  <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                    <i class="fa fa-users text-lg opacity-10" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row mt-xl-4 mb-4">
        <div class="col-lg-6 mb-4">
          <div class="card z-index-2">
            <div class="card-header pb-0">
              <h6>Statistik Tamu <?= Date('Y'); ?> </h6>
            </div>
            <div class="card-body p-3">
              <div class="chart">
                <canvas id="statistik-tamu" class="chart-canvas" height="300"></canvas>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-6">
          <div class="card z-index-2">
            <div class="card-header pb-0">
              <h6>Statistik Member <?= Date('Y'); ?> </h6>
            </div>
            <div class="card-body p-3">
              <div class="chart">
                <canvas id="statistik-member" class="chart-canvas" height="300"></canvas>
              </div>
            </div>
          </div>
        </div>
      </div>

      <script src="<?= base_url(); ?>/assets/template/js/plugins/chartjs.min.js"></script>

      <!-- aktifkan datatables table transaksi -->
      <script>
        $(document).ready(function() {
          $('#transaksi').DataTable();
        });
      </script>


      <!-- chart statistik tamu -->
      <script>
        var ctx2 = document.getElementById("statistik-tamu");

        new Chart(ctx2, {
          type: "line",
          data: {
            labels: [<?= $labelsTamu; ?>],
            datasets: [{
                label: "Tamu",
                tension: 0.4,
                borderColor: "#cb0c9f",
                borderWidth: 3,
                data: [<?= $dataTamu; ?>],
                maxBarThickness: 6
              },

            ],
          },
          options: {
            responsive: true,
            maintainAspectRatio: false,
            plugins: {
              legend: {
                display: false,
              }
            },
            tooltips: {
              enabled: true,
              mode: "index",
              intersect: false,
            },
            scales: {
              yAxes: [{
                gridLines: {
                  borderDash: [2],
                  borderDashOffset: [2],
                  color: '#dee2e6',
                  zeroLineColor: '#dee2e6',
                  zeroLineWidth: 1,
                  zeroLineBorderDash: [2],
                  drawBorder: false,
                },
                ticks: {
                  suggestedMin: 0,
                  suggestedMax: 500,
                  beginAtZero: true,
                  padding: 10,
                  fontSize: 11,
                  fontColor: '#adb5bd',
                  lineHeight: 3,
                  fontStyle: 'normal',
                  fontFamily: "Open Sans",
                },
              }, ],
              xAxes: [{
                gridLines: {
                  zeroLineColor: 'rgba(0,0,0,0)',
                  display: false,
                },
                ticks: {
                  padding: 10,
                  fontSize: 11,
                  fontColor: '#adb5bd',
                  lineHeight: 3,
                  fontStyle: 'normal',
                  fontFamily: "Open Sans",
                },
              }, ],
            },
          },
        });
      </script>

      <!-- chart statistik member -->
      <script>
        var ctx2 = document.getElementById("statistik-member");

        new Chart(ctx2, {
          type: "line",
          data: {
            labels: [<?= $labelsMember; ?>],
            datasets: [{
                label: "Member",
                tension: 0.2,
                borderColor: "#3A416F",
                borderWidth: 3,
                data: [<?= $dataMember; ?>],
                maxBarThickness: 6
              },

            ],
          },
          options: {
            responsive: true,
            maintainAspectRatio: false,
            plugins: {
              legend: {
                display: false,
              }
            },
            tooltips: {
              enabled: true,
              mode: "index",
              intersect: false,
            },
            scales: {
              yAxes: [{
                gridLines: {
                  borderDash: [2],
                  borderDashOffset: [2],
                  color: '#dee2e6',
                  zeroLineColor: '#dee2e6',
                  zeroLineWidth: 1,
                  zeroLineBorderDash: [2],
                  drawBorder: false,
                },
                ticks: {
                  suggestedMin: 0,
                  suggestedMax: 500,
                  beginAtZero: true,
                  padding: 10,
                  fontSize: 11,
                  fontColor: '#adb5bd',
                  lineHeight: 3,
                  fontStyle: 'normal',
                  fontFamily: "Open Sans",
                },
              }, ],
              xAxes: [{
                gridLines: {
                  zeroLineColor: 'rgba(0,0,0,0)',
                  display: false,
                },
                ticks: {
                  padding: 10,
                  fontSize: 11,
                  fontColor: '#adb5bd',
                  lineHeight: 3,
                  fontStyle: 'normal',
                  fontFamily: "Open Sans",
                },
              }, ],
            },
          },
        });
      </script>