<?php if (isset($_SESSION['username'])) {
  header('Location: dashboard.php');
} ?>
<main class="main-content mt-0">
  <section>
    <div class="page-header min-vh-100">
      <div class="container">
        <div class="row">
          <div class="col-xl-4 col-lg-5 col-md-6 d-flex flex-column mx-auto">
            <div class="card card-plain">
              <div class="card-header py-0 text-left bg-transparent" style="margin-top: -30px;">
                <img src="<?= base_url('assets'); ?>/images/logo.svg" alt="Logo Phylot" width="200" style="margin-bottom: -30px;">
                <h3 class="font-weight-bolder text-info text-gradient">Welcome back</h3>
                <p class="mb-0 text-sm">Enter your email and password to sign in</p>
              </div>
              <div class="card-body">
                <form role="form" method="POST" action="<?= base_url('/auth/login'); ?>" class="needs-validation" novalidate>
                  <?= csrf_field(); ?>
                  <?php if (!empty(session()->getFlashdata('fail'))) : ?>
                    <div class="alert alert-danger text-white" role="alert"><?= session()->getFlashdata('fail'); ?></div>
                  <?php endif; ?>
                  <label>Username</label>
                  <div class="mb-1">
                    <input type="text" name="username" value="<?= set_value('username'); ?>" class="form-control" placeholder="Masukkan Username">
                    <span class="text-danger"><?= isset($validation) ? display_error($validation, 'username') : ''; ?></span>
                  </div>
                  <label>Password</label>
                  <div class="mb-1">
                    <input type="password" name="password" value="<?= set_value('password'); ?>" class="form-control" placeholder="Masukkan Password">
                    <span class="text-danger"><?= isset($validation) ? display_error($validation, 'password') : ''; ?></span>
                  </div>
                  <label for="level">Level User</label>
                  <div class="mb-1">
                    <select class="form-select" id="level" name="level" aria-label="level user">
                      <option value="">Pilih level user</option>
                      <option <?php (set_value('level') == 'Owner') ? 'selected' : ''; ?> value="Owner">Pengelola</option>
                      <option <?php (set_value('level') == 'Petugas') ? 'selected' : ''; ?> value="Petugas">Petugas</option>
                    </select>
                    <span class="text-danger"><?= isset($validation) ? display_error($validation, 'level') : ''; ?></span>

                  </div>
                  <div class="text-center">
                    <button type="submit" name="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Sign in</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="oblique position-absolute top-0 h-100 d-md-block d-none me-n8">
              <div class="oblique-image bg-cover position-absolute fixed-top ms-auto h-100 z-index-0 ms-n6" style="background-image:url('<?= base_url(); ?>/assets/template/img/curved-images/curved6.jpg')"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>