<body class="g-sidenav-show bg-gray-100">
  <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-radius-xl border-0 my-3 fixed-start ms-3" id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href="<?= base_url('dashboard'); ?>">
        <img src="<?= base_url(); ?>/assets/images/logo.svg" class="navbar-brand-img h-100" alt="main_logo">
        <span class="ms-1 font-weight-bold text-info text-gradient">PhylotCluster</span>
      </a>
    </div>
    <hr class="horizontal dark mt-0">
    <div class="collapse navbar-collapse max-height-vh-100 h-50 w-auto" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <?php
        if (session()->level == 'Petugas') :
        ?>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('dashboard'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-spaceship text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="<?= base_url('stream'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-button-play text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Streaming</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link  " href="<?= base_url('master'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-chart-bar-32 text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Master</span>
            </a>
          </li>
        <?php
        endif;
        ?>
        <?php
        if (session()->level == 'Owner') :
        ?>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('dashboard'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-spaceship text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="<?= base_url('stream'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-button-play text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Streaming</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('master'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-chart-bar-32 text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Master</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link  " href="../pages/virtual-reality.html">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-archive-2 text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Report</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link  " href="../pages/rtl.html">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-books text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Log</span>
            </a>
          </li>
        <?php
        endif;
        ?>
        <li class="nav-item">
          <a class="nav-link" href="<?= base_url('akun') . '/' . session()->username; ?>">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-single-02 text-dark"></i>
            </div>
            <span class="nav-link-text ms-1">Profile</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link  " href="<?= base_url('auth/logout'); ?>">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-button-power text-dark"></i>
            </div>
            <span class="nav-link-text ms-1">Logout</span>
          </a>
        </li>
      </ul>
    </div>
    <div class="sidenav-footer mx-3 my-3">
      <div class="card card-background shadow-none card-background-mask-secondary" id="sidenavCard">
        <div class="full-background" style="background-image: url('<?= base_url(); ?>/assets/template/img/curved-images/white-curved.jpeg')"></div>
        <div class="card-body text-start p-3 w-100">
          <div class="icon icon-shape icon-sm bg-white shadow text-center mb-3 d-flex align-items-center justify-content-center border-radius-md">
            <i class="fa fa-whatsapp text-dark text-gradient text-lg top-0" aria-hidden="true" id="sidenavCardIcon"></i>
          </div>
          <div class="docs-info">
            <h6 class="text-white up mb-0">Call Support</h6>
            <p class="text-xs font-weight-bold">Please contact us in WhatsApp.</p>
            <a href="https://wa.me/6282287778685" target="_blank" class="btn btn-white w-100 mb-0">Click here</a>
          </div>
        </div>
      </div>
    </div>
  </aside>
  <main class="main-content position-relative max-height-vh-100 h-100">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none" id="navbarBlur" navbar-scroll="true">
      <div class="container-fluid py-1 px-3">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark" aria-current="page">Streaming</li>
          </ol>
        </nav>
        <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
          <div class="ms-md-auto pe-md-3 d-flex align-items-center">
          </div>
          <ul class="navbar-nav  justify-content-end">
            <li class="nav-item d-xl-none d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </a>
            </li>
            <li class="nav-item d-flex align-items-center ps-4">
              <a href="<?= base_url('akun') . '/' . $_SESSION['username'] ?>" class="btn btn-tooltip mb-0 bg-gradient-primary text-white px-3" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Profile" data-container="body" data-animation="true">
                <i class="fa fa-user me-sm-1"></i>
                <span class="d-sm-inline d-none">Akun</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- End Navbar -->

    <?php if (!empty(session()->getFlashdata('delete_cam'))) : ?>
      <div class="alert alert-success mx-4 my-2 text-white" role="alert"><?= session()->getFlashdata('delete_cam'); ?></div>
    <?php endif; ?>

    <div class="container-fluid py-4">
      <div class="row mb-4">
        <div class="col-md-3">
          <button class="btn btn-icon btn-3 bg-gradient-primary mb-0 ms-2 me-4" type="button" data-bs-toggle="modal" data-bs-target="#inputDataModal">
            <span class="btn-inner--text me-1">Tambah Camera</span>
            <span class="btn-inner--icon"><i class="ni ni-bold-right"></i></span>
          </button>
          <!-- Tambah Cam Modal -->
          <div class="modal fade" id="inputDataModal" tabindex="-1" role="dialog" aria-labelledby="inputDataModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="inputDataModalLabel">Input Data Camera</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form action="<?= base_url('cam/add'); ?>" method="POST">
                    <div class="form-group">
                      <label for="cam_name">Nama Camera</label>
                      <input type="text" class="form-control" id="cam_name" name="cam_name">
                    </div>

                    <div class="form-group">
                      <label for="url">Url Stream</label>
                      <input type="text" class="form-control" id="url" name="url">
                    </div>
                    <div class="modal-footer">
                      <button type="reset" class="btn bg-gradient-secondary" data-bs-dismiss="modal">Batal</button>
                      <button type="submit" class="btn bg-gradient-primary">Simpan</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <?php
        if (empty($cam)) :
        ?>
          <div class="container d-flex align-midle text-center flex-column p-3">
            <div>
              <img src="<?= base_url('assets/images'); ?>/no-data.svg" alt="" width="300">
            </div>
            <div>
              <h5 style="margin-top: -20px; margin-bottom: 50px;">Belum ada data</h5>
            </div>
          </div>
        <?php
        endif;
        ?>

        <?php
        foreach ($cam as $c) :
        ?>
          <div class="col-sm-6">
            <div class="card mb-4 p-3">
              <div class="card-header pb-0 d-flex justify-content-center align-midle">
                <h6><?= $c['nama_camera']; ?></h6>
                <input type="hidden" name="id_cam" id="id_cam" value="<?= $c['id']; ?>">
                <button class="btn btn-icon py-2 px-3 btn-3 bg-gradient-primary mb-2 ms-3" type="button" data-bs-toggle="modal" data-bs-target="#editDataModal<?= $c['id'] ?>">
                  <span class="btn-inner--icon"><i class="fa fa-pencil"></i></span>
                </button>
                <a class="btn text-danger py-2 px-3 btn-3 mb-2 ms-3" href="<?= base_url('cam/delete') . '/' . $c['id']; ?>" name="btn-hapus" id="btn-hapus">
                  <span class="btn-inner--icon"><i class="fa fa-trash"></i></span>
                </a>

                <!-- Modal -->
                <div class="modal fade" id="editDataModal<?= $c['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="editDataModalLabel<?= $c['id'] ?>" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="editDataModalLabel<?= $c['id'] ?>">Edit Data Camera</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <form action="<?= base_url('cam/update' . '/' . $c['id']); ?>" method="POST">
                          <input type="hidden" class="form-control" id="id" name="id" value="<?= $c['id']; ?>">
                          <div class="form-group">
                            <label for="edit_name">Nama Camera</label>
                            <input type="text" class="form-control" id="edit_name" name="edit_name" value="<?= $c['nama_camera']; ?>">
                          </div>

                          <div class="form-group">
                            <label for="edit_url">Url</label>
                            <input type="text" class="form-control" id="edit_url" name="edit_url" value="<?= $c['url_stream']; ?>">

                          </div>
                          <div class="modal-footer">
                            <button type="reset" class="btn bg-gradient-secondary" data-bs-dismiss="modal">Batal</button>
                            <button type="submit" class="btn bg-gradient-primary">Simpan</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body px-0 pt-0 pb-4 mx-auto">
                <img src="<?= base_url('assets/images/'); ?>/default.jpg" class="cam-holder" alt="Cam Holder">
              </div>
            </div>
          </div>
        <?php
        endforeach;
        ?>
      </div>