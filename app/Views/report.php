<body class="g-sidenav-show bg-gray-100">
  <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-radius-xl border-0 my-3 fixed-start ms-3" id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href="<?= base_url('dashboard'); ?>">
        <img src="<?= base_url(); ?>/assets/images/logo.svg" class="navbar-brand-img h-100" alt="main_logo">
        <span class="ms-1 font-weight-bold text-info text-gradient">PhylotCluster</span>
      </a>
    </div>
    <hr class="horizontal dark mt-0">
    <div class="collapse navbar-collapse max-height-vh-100 h-50 w-auto" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <?php

        if (session()->level == 'Petugas') :
        ?>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('dashboard'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-spaceship text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('stream'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-button-play text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Streaming</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link  " href="<?= base_url('master'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-chart-bar-32 text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Master</span>
            </a>
          </li>
        <?php
        endif;
        ?>
        <?php
        if (session()->level == 'Owner') :
        ?>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('dashboard'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-spaceship text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('stream'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-button-play text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Streaming</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('master'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-chart-bar-32 text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Master</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="<?= base_url('report'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-archive-2 text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Report</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('log'); ?>">
              <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-books text-dark"></i>
              </div>
              <span class="nav-link-text ms-1">Log</span>
            </a>
          </li>
        <?php
        endif;
        ?>
        <li class="nav-item">
          <a class="nav-link" href="<?= base_url('akun') . '/' . session()->username; ?>">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-single-02 text-dark"></i>
            </div>
            <span class="nav-link-text ms-1">Profile</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link  " href="<?= base_url('auth/logout'); ?>">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-button-power text-dark"></i>
            </div>
            <span class="nav-link-text ms-1">Logout</span>
          </a>
        </li>
      </ul>
    </div>
    <div class="sidenav-footer mx-3 my-3">
      <div class="card card-background shadow-none card-background-mask-secondary" id="sidenavCard">
        <div class="full-background" style="background-image: url('<?= base_url(); ?>/assets/template/img/curved-images/white-curved.jpeg')"></div>
        <div class="card-body text-start p-3 w-100">
          <div class="icon icon-shape icon-sm bg-white shadow text-center mb-3 d-flex align-items-center justify-content-center border-radius-md">
            <i class="fa fa-whatsapp text-dark text-gradient text-lg top-0" aria-hidden="true" id="sidenavCardIcon"></i>
          </div>
          <div class="docs-info">
            <h6 class="text-white up mb-0">Call Support</h6>
            <p class="text-xs font-weight-bold">Please contact us in WhatsApp.</p>
            <a href="https://wa.me/6282287778685" target="_blank" class="btn btn-white w-100 mb-0">Click here</a>
          </div>
        </div>
      </div>
    </div>
  </aside>
  <main class="main-content position-relative max-height-vh-100 h-100">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none" id="navbarBlur" navbar-scroll="true">
      <div class="container-fluid py-1 px-3">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark" aria-current="page">Report</li>
          </ol>
        </nav>
        <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
          <div class="ms-md-auto pe-md-3 d-flex align-items-center">
          </div>
          <ul class="navbar-nav  justify-content-end">
            <li class="nav-item d-xl-none d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </a>
            </li>
            <li class="nav-item d-flex align-items-center ps-4">
              <a href="<?= base_url('akun') . '/' . session()->username ?>" class="btn btn-tooltip mb-0 bg-gradient-primary text-white px-3" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Profile" data-container="body" data-animation="true">
                <i class="fa fa-user me-sm-1"></i>
                <span class="d-sm-inline d-none">Akun</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- End Navbar -->

    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              <h6>Report Data Transaksi</h6>
            </div>
            <div class="card-body px-4 pt-0 pb-2">

              <!-- Nav tabs -->
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                  <button class="nav-link active" id="transin-tab" data-bs-toggle="tab" data-bs-target="#transin" type="button" role="tab" aria-controls="transin" aria-selected="true">Transaksi Masuk</button>
                </li>
                <li class="nav-item" role="presentation">
                  <button class="nav-link" id="transout-tab" data-bs-toggle="tab" data-bs-target="#transout" type="button" role="tab" aria-controls="transout" aria-selected="false">Transaksi Keluar</button>
                </li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div class="tab-pane fade show active" id="transin" role="tabpanel" aria-labelledby="transin-tab">
                  <div class="table-responsive pb-3 mt-4">
                    <input type="hidden" name="<?= csrf_token(); ?>" value="<?= csrf_hash(); ?>" class="txt_csrfname">
                    <table class="display table align-items-center mb-0" id="transin-table">
                      <thead>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">RFID</th>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Foto Masuk</th>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Waktu Masuk</th>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Plat</th>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Keperluan</th>
                      </thead>

                    </table>
                  </div>
                </div>

                <div class="tab-pane fade" id="transout" role="tabpanel" aria-labelledby="transout-tab">
                  <div class="table-responsive p-0 pb-3 mt-4">
                    <table class="table align-items-center mb-0" style="width: 100%;" id="transout-table">
                      <thead>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">RFID</th>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Foto Keluar</th>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Waktu Keluar</th>
                      </thead>

                    </table>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>

    <script>
      $(document).ready(function() {
        function newexportaction(e, dt, button, config) {
          var self = this;
          var oldStart = dt.settings()[0]._iDisplayStart;
          dt.one('preXhr', function(e, s, data) {
            // Just this once, load all data from the server...
            data.start = 0;
            data.length = $('#transin-table').DataTable().page.info().recordsTotal;
            dt.one('preDraw', function(e, settings) {
              // Call the original action function
              if (button[0].className.indexOf('buttons-copy') >= 0) {
                $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
              } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                  $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
                  $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
              } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                  $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
                  $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
              } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                  $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
                  $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
              } else if (button[0].className.indexOf('buttons-print') >= 0) {
                $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
              }
              dt.one('preXhr', function(e, s, data) {
                // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                // Set the property to what it was before exporting.
                settings._iDisplayStart = oldStart;
                data.start = oldStart;
              });
              // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
              setTimeout(dt.ajax.reload, 0);
              // Prevent rendering of the full data to the DOM
              return false;
            });
          });
          // Requery the server with the new one-time export settings
          dt.ajax.reload();
        };

        $('#transin-table').DataTable({
          'processing': true,
          'serverSide': true,
          'pagingType': 'full_numbers',
          'deferRender': true,
          'scrollY': 400,
          "lengthMenu": [
            [10, 25, 50, 100, 1000, 10000, -1],
            [10, 25, 50, 100, 1000, 10000, "All"]
          ],
          'dom': '<"top"Blf>rt<"bottom"ip><"clear">',
          "order": [
            [2, "desc"]
          ],
          'serverMethod': 'post',
          'ajax': {
            'url': "<?= base_url('report/getTransIn') ?>",
            'data': function(data) {
              var csrfName = $('.txt_csrfname').attr('name');
              var csrfHash = $('.txt_csrfname').val();

              return {
                data: data,
                [csrfName]: csrfHash
              }
            },
            'dataSrc': function(data) {
              $('.txt_csrfname').val(data.token);

              return data.aaData;
            }
          },
          'columns': [{
              data: 'rfid_masuk'
            },
            {
              data: 'fotomasuk',
              render: function(data, type, row, meta) {
                return '<img src="<?= base_url('assets/images') . '/' ?>' + data + '" width="150"/>';
              }
            },
            {
              data: 'tglmasuk'
            },
            {
              data: 'keperluan' ? 'keperluan' : ''
            },
            {
              data: 'plat'
            },
          ],
          "buttons": [{
              "extend": 'copy',
              "text": '<i class="fa fa-files-o" style="color: white;"></i>',
              "titleAttr": 'Copy',
              columns: [0, 2, 3, 4]
            },
            {
              "extend": 'excelHtml5',
              "text": '<i class="fa fa-file-excel-o" style="color: white;"></i>',
              "titleAttr": 'Excel',
              columns: [0, 2, 3, 4]
            },
            {
              "extend": 'csv',
              "text": '<i class="fa fa-file-text-o" style="color: white;"></i>',
              "titleAttr": 'CSV',
              columns: [0, 2, 3, 4]
            },
            {
              "extend": 'pdfHtml5',
              "text": '<i class="fa fa-file-pdf-o" style="color: white;"></i>',
              "titleAttr": 'PDF',
              columns: [0, 2, 3, 4]
            },
            {
              "extend": 'print',
              "text": '<i class="fa fa-print" style="color: white;"></i>',
              "titleAttr": 'Print',
              "autoPrint": true,
              "title": 'Laporan Kendaraan Masuk',
              "columns": [0, 1, 2, 3, 4],
              "exportOptions": {
                stripHtml: false,
                "modifier": {
                  "page": 'all'
                }
              },
              customize: function(win) {
                $(win.document.body).find('table').addClass('display').css('font-size', '20px');
                $(win.document.body).find('tr:nth-child(odd) td').each(function(index) {
                  $(this).css('background-color', '#ebebeb');
                });
                $(win.document.body).find('h1').css('text-align', 'center');
                $(win.document.body).find('h1').css('margin', '20px 0');
              }
            }
          ],
        });
      });
    </script>