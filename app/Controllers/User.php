<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\AuthModel;

class User extends BaseController
{
    public function index($username)
    {
        $model = new AuthModel();
        $username = session()->get('username');
        $data['user'] = $model->where('username', $username)->first();
        echo view('templates/header');
        echo view('profile', $data);
        echo view('templates/footer');
    }
}
