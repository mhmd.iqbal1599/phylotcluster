<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\AuthModel;
use App\Libraries\Hash;
use App\Models\LogModel;
use CodeIgniter\I18n\Time;

class Auth extends BaseController
{

    public function __construct()
    {
        helper(['url', 'form']);
    }

    public function index()
    {
        echo view('templates/header_auth');
        echo view('auth/sign_in');
        echo view('templates/footer_auth');
    }

    public function login()
    {
        $model = new AuthModel();
        $validation = $this->validate([
            'username' => [
                'rules' => 'required|min_length[4]|is_not_unique[masterdata.username]',
                'errors' => [
                    'required' => 'Username tidak boleh kosong',
                    'min_length' => 'Username setidaknya terdiri dari 4 huruf',
                    'is_not_unique' => 'Username yang Anda masukkan tidak ditemukan pada database'
                ]
            ],
            'password' => [
                'rules' => 'required|min_length[6]',
                'errors' => [
                    'required' => 'Password tidak boleh kosong',
                    'min_length' => 'Password setidaknya terdiri dari 6 huruf',
                ]
            ],
            'level' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Pilih level user terlebih dahulu',
                ]
            ],
        ]);

        if (!$validation) {
            echo view('templates/header_auth');
            echo view('auth/sign_in', ['validation' => $this->validator]);
            echo view('templates/footer_auth');
        } else {
            $username = $this->request->getPost('username');
            $password = $this->request->getPost('password');
            $level = $this->request->getPost('level');
            $user = $model->where('username', $username)->first();
            $check_password = Hash::check($password, $user['password']);

            if ($check_password && $user['level'] == $level) {
                if ($user['status'] == 'Aktif') {
                    session()->setFlashdata('success', 'Login berhasil.');

                    $logModel = new LogModel();
                    $template = '$nama_petugas melakukan login pada $waktu';
                    $vars = array(
                        '$nama_petugas' => $user['nama'],
                        '$waktu' => Time::now()
                    );

                    $logText = strtr($template, $vars);
                    $logData = [
                        'nama_petugas' => $user['nama'],
                        'deskripsi' => $logText,
                        'created_at' => Time::now(),
                    ];

                    $logModel->insert($logData);

                    $userdata = [
                        'id' => $user['id'],
                        'username' => $user['username'],
                        'nama' => $user['nama'],
                        'level' => $user['level'],
                    ];
                    session()->set($userdata);

                    return redirect()->to('/dashboard');
                } else {
                    session()->setFlashdata('fail', 'Akun Anda telah dinonaktifkan.');
                    return redirect()->back()->withInput();
                }
            } else {
                session()->setFlashdata('fail', 'Data yang Anda masukkan tidak ditemukan pada database.');
                return redirect()->back()->withInput();
            }
        }
    }

    public function logout()
    {
        $session = session();

        $logModel = new LogModel();
        $template = '$nama_petugas melakukan logout pada $waktu';
        $vars = array(
            '$nama_petugas' => $session->nama,
            '$waktu' => Time::now()
        );

        $logText = strtr($template, $vars);
        $logData = [
            'nama_petugas' => $session->nama,
            'deskripsi' => $logText,
            'created_at' => Time::now(),
        ];

        $logModel->insert($logData);

        $session->destroy();

        return redirect()->to('/');
    }
}
