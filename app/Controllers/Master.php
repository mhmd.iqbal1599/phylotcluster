<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\AuthModel;
use CodeIgniter\I18n\Time;
use App\Libraries\Hash;

class Master extends BaseController
{

    public function __construct()
    {
        helper(['url', 'form']);
    }

    public function index()
    {
        $model = new AuthModel();
        $data['master'] = $model->orderBy('nama', 'asc')->whereNotIn('level', ['Owner'])->findAll();

        echo view('templates/header');
        echo view('master/index', $data);
        echo view('templates/footer');
    }

    public function input_page()
    {
        echo view('templates/header');
        echo view('master/input');
        echo view('templates/footer');
    }

    public function test()
    {
        echo view('templates/header_auth');
        echo view('master/input');
        echo view('templates/footer_auth');
    }

    public function edit_page($id)
    {
        $model = new AuthModel();
        $data['master'] = $model->where('id', $id)->first();

        echo view('templates/header');
        echo view('master/edit', $data);
        echo view('templates/footer');
    }

    public function save()
    {
        $validation = $this->validate([
            'username' => [
                'rules' => 'is_unique[masterdata.username]|required',
                'errors' => [
                    'is_unique' => 'Username ini sudah digunakan user lain. Harap gunakan username lain',
                    'required'  => 'Username tidak boleh kosong',
                ]
            ],
            'fullname' => [
                'rules' => 'required',
                'errors' => [
                    'required'  => 'Nama lengkap tidak boleh kosong',
                ]
            ],
            'rfid' => [
                'rules' => 'required|max_length[16]|min_length[10]',
                'errors' => [
                    'required'  => 'RFID tidak boleh kosong',
                    'max_length'  => 'RFID seharusnya tidak lebih dari 16 angka',
                    'min_length'  => 'RFID seharusnya terdiri dari 10 hingga 16 angka',
                ]
            ],
            'level' => [
                'rules' => 'required',
                'errors' => [
                    'required'  => 'Harap pilih level user',
                ]
            ],
            'address' => [
                'rules' => 'required|max_length[150]|min_length[10]',
                'errors' => [
                    'required'  => 'Alamat tidak boleh kosong',
                    'max_length'  => 'Panjang maksimal Alamat adalah 150 karakter',
                    'min_length'  => 'Panjang minimal Alamat adalah 10 karakter',
                ]
            ],
            'plat' => [
                'rules' => 'required|max_length[12]',
                'errors' => [
                    'required'  => 'Plat Kendaraan tidak boleh kosong',
                    'max_length'  => 'Plat Kendaraan seharusnya tidak lebih dari 9 karakter',
                ]
            ],
            'phone' => [
                'rules' => 'required|max_length[13]|min_length[11]|numeric',
                'errors' => [
                    'required'  => 'No. HP tidak boleh kosong',
                    'max_length'  => 'No. HP seharusnya tidak lebih dari 13 angka',
                    'min_length'  => 'No. HP yang Anda masukkan tidak valid, mohon periksa kembali',
                    'numeric'  => 'Hanya angka yang diperbolehkan',
                ]
            ],
        ]);

        $model = new AuthModel();
        if (!$validation) {
            echo view('templates/header');
            echo view('master/input', ['validation' => $this->validator]);
            echo view('templates/footer');
        } else {
            $username = $this->request->getPost('username');
            $password = $this->request->getPost('password');
            $fullname = $this->request->getPost('fullname');
            $level = $this->request->getPost('level');
            $rfid = $this->request->getPost('rfid');
            $address = $this->request->getPost('address');
            $plat = $this->request->getPost('plat');
            $phone = $this->request->getPost('phone');

            if ($password != '') {
                $data = [
                    'username' => $username,
                    'password' => Hash::make($password),
                    'nama' => $fullname,
                    'level' => $level,
                    'rfid' => $rfid,
                    'alamat' => $address,
                    'plat' => $plat,
                    'nohp' => $phone,
                    'tgldaftar' => Time::now('Asia/Jakarta', 'id-ID'),
                    'status' => 'Aktif'
                ];
            } else {
                $password = '123456';
                $data = [
                    'username' => $username,
                    'password' => Hash::make($password),
                    'nama' => $fullname,
                    'level' => $level,
                    'rfid' => $rfid,
                    'alamat' => $address,
                    'plat' => $plat,
                    'nohp' => $phone,
                    'tgldaftar' => Time::now('Asia/Jakarta', 'id-ID'),
                    'status' => 'Aktif'
                ];
            }

            $query = $model->insert($data);
            if ($query) {
                session()->setFlashdata('success', 'Data berhasil ditambahkan.');
                return redirect()->to('/master');
            } else {
                session()->setFlashdata('fail', 'Data gagal disimpan.');
                return redirect()->back()->withInput();
            }
        }
    }


    public function update($id)
    {
        $model = new AuthModel();
        $user = $model->where('id', $id)->first();

        $id = $user['id'];

        $validation = $this->validate([
            'password2' => [
                'rules' => 'matches[password]',
                'errors' => [
                    'matches'  => 'Password yang dimasukkan tidak cocok',
                ]
            ],
            'fullname' => [
                'rules' => 'required',
                'errors' => [
                    'required'  => 'Nama lengkap tidak boleh kosong',
                ]
            ],
            'rfid' => [
                'rules' => 'required|max_length[16]|min_length[10]',
                'errors' => [
                    'required'  => 'RFID tidak boleh kosong',
                    'max_length'  => 'RFID seharusnya tidak lebih dari 16 angka',
                    'min_length'  => 'RFID seharusnya terdiri dari 10 hingga 16 angka',
                ]
            ],
            'level' => [
                'rules' => 'required',
                'errors' => [
                    'required'  => 'Harap pilih level user',
                ]
            ],
            'address' => [
                'rules' => 'required|max_length[150]|min_length[10]',
                'errors' => [
                    'required'  => 'Alamat tidak boleh kosong',
                    'max_length'  => 'Panjang maksimal Alamat adalah 150 karakter',
                    'min_length'  => 'Panjang minimal Alamat adalah 10 karakter',
                ]
            ],
            'plat' => [
                'rules' => 'required|max_length[12]',
                'errors' => [
                    'required'  => 'Plat Kendaraan tidak boleh kosong',
                    'max_length'  => 'Plat Kendaraan seharusnya tidak lebih dari 9 karakter',
                ]
            ],
            'phone' => [
                'rules' => 'required|max_length[13]|min_length[11]|numeric',
                'errors' => [
                    'required'  => 'No. HP tidak boleh kosong',
                    'max_length'  => 'No. HP seharusnya tidak lebih dari 13 angka',
                    'min_length'  => 'No. HP yang Anda masukkan tidak valid, mohon periksa kembali',
                    'numeric'  => 'Hanya angka yang diperbolehkan',
                ]
            ],
            'status' => [
                'rules' => 'required',
                'errors' => [
                    'required'  => 'Pilih status terlebih dahulu',
                ]
            ],
        ]);

        if (!$validation) {
            return redirect()->to('/master/edit' . $id)->withInput();
        } else {
            $username = $user['username'];
            $password = $this->request->getPost('password');
            $password2 = $this->request->getPost('password2');
            $password_lama = $this->request->getPost('passwordLama');
            $fullname = $this->request->getPost('fullname');
            $level = $this->request->getPost('level');
            $rfid = $this->request->getPost('rfid');
            $address = $this->request->getPost('address');
            $plat = $this->request->getPost('plat');
            $phone = $this->request->getPost('phone');
            $tgl = Time::now('Asia/Jakarta', 'id-ID');
            $status = $this->request->getPost('status');

            if ($password !== '' && $password2 !== '') {
                $update_data = [
                    'username' => $username,
                    'password' => Hash::make($password2),
                    'nama' => $fullname,
                    'level' => $level,
                    'rfid' => $rfid,
                    'alamat' => $address,
                    'plat' => $plat,
                    'nohp' => $phone,
                    'tgldaftar' => $tgl,
                    'status' => $status
                ];
            } else {
                $update_data = [
                    'username' => $username,
                    'password' => $password_lama,
                    'nama' => $fullname,
                    'level' => $level,
                    'rfid' => $rfid,
                    'alamat' => $address,
                    'plat' => $plat,
                    'nohp' => $phone,
                    'tgldaftar' => $tgl,
                    'status' => $status
                ];
            }

            $query = $model->update($id, $update_data);
            if ($query) {
                session()->setFlashdata('success', 'Data berhasil diperbarui.');
                return redirect()->to('/master');
            }
        }
    }
}
