<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\TransactionModel;
use App\Models\TransactionOutModel;

class Report extends BaseController
{
    public function index()
    {


        echo view('templates/header-dashboard');
        echo view('report');
        echo view('templates/footer');
    }

    public function getTransIn()
    {
        $request = service('request');
        $postData = $request->getPost();
        $dtpostData = $postData['data'];

        $draw = $dtpostData['draw'];
        $start = $dtpostData['start'];
        $rowperpage = $dtpostData['length'];
        $columnIndex = $dtpostData['order'][0]['column'];
        $columnName = $dtpostData['columns'][$columnIndex]['data'];
        $columnSortOrder = $dtpostData['order'][0]['dir'];
        $searchValue = $dtpostData['search']['value'];

        $modelTransMasuk = new TransactionModel();
        $totalRecords = $modelTransMasuk->select('id')->countAllResults();
        $totalRecordwithFilter = $modelTransMasuk->select('id')
            ->orLike('rfid_masuk', $searchValue)
            ->orLike('tglmasuk', $searchValue)
            ->orLike('plat', $searchValue)
            ->orLike('keperluan', $searchValue)
            ->countAllResults();

        $records = $modelTransMasuk->select('*')
            ->orLike('rfid_masuk', $searchValue)
            ->orLike('tglmasuk', $searchValue)
            ->orLike('plat', $searchValue)
            ->orLike('keperluan', $searchValue)
            ->orderBy($columnName, $columnSortOrder)
            ->findAll($rowperpage, $start);

        $data = array();
        foreach ($records as $record) {
            $data[] = array(
                'rfid_masuk' => $record['rfid_masuk'],
                'fotomasuk' => $record['fotomasuk'],
                'tglmasuk' => $record['tglmasuk'],
                'keperluan' => $record['keperluan'],
                'plat' => $record['plat'],
            );
        }

        $response = array(
            'draw' => intval($draw),
            'iTotalRecords' => $totalRecords,
            'iTotalDisplayRecords' => $totalRecordwithFilter,
            'aaData' => $data,
            'token' => csrf_hash()
        );

        return $this->response->setJSON($response);
    }
}
