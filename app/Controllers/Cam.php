<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\CamModel;

class Cam extends BaseController
{
    public function __construct()
    {
        $this->model = new CamModel();
    }
    public function index()
    {
        $data['cam'] = $this->model->findAll();
        echo view('templates/header');
        echo view('cam', $data);
        echo view('templates/footer');
    }

    public function show()
    {
        $data = $this->model->findAll();
        echo json_encode($data);
    }

    public function add()
    {
        $name = $this->request->getPost('cam_name');
        $url = $this->request->getPost('url');
        $data = [
            'nama_camera' => $name,
            'url_stream' => $url,
        ];

        $save = $this->model->insert($data);
        if ($save) {
            session()->setFlashdata('success', 'Data Camera berhasil ditambah.');
            return redirect()->to('/stream');
        }
    }

    public function update($id)
    {
        $id = $this->request->getPost('id');
        $name = $this->request->getPost('edit_name');
        $url = $this->request->getPost('edit_url');
        $data = [
            'nama_camera' => $name,
            'url_stream' => $url,
        ];

        $save = $this->model->update($id, $data);
        if ($save) {
            session()->setFlashdata('success', 'Data Camera berhasil diperbarui.');
            return redirect()->to('/stream');
        }
    }

    public function delete($id)
    {
        $delete = $this->model->where('id', $id)->delete();

        if ($delete) {
            session()->setFlashdata('delete_cam', 'Data Camera dihapus.');
            return redirect()->to('/stream');
        }
    }
}
