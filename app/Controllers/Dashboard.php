<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\AuthModel;
use App\Models\LogModel;
use App\Models\TransactionModel;
use CodeIgniter\I18n\Time;

class Dashboard extends BaseController
{
    public function __construct()
    {

        $db = \Config\Database::connect();
        $this->builder = $db->table('transaksi');
    }
    public function index()
    {

        $dataArrayTamu = $this->count_tamu_member_tahunan('Petugas')->getResultArray();
        foreach ($dataArrayTamu as $values) {
            $labelsTamu[] = $values['bulan'];
            $dataTamu[] = $values['jumlah'];
        };

        $dataArrayMember = $this->count_tamu_member_tahunan('Member')->getResultArray();
        foreach ($dataArrayMember as $values) {
            $labelsMember[] = $values['bulan'];
            $dataMember[] = $values['jumlah'];
        };

        $model = new TransactionModel();
        $date = date('Y-m-d');
        $data =
            [
                'transaction' => $model
                    ->where('keperluan', NULL)
                    ->like('tglmasuk', $date)
                    ->orderBy('tglmasuk', 'asc')
                    ->findAll(),
                'c_tamu_harian'                     => $this->count_tamu_harian(),
                'c_tamu_bulanan'                    => $this->count_tamu_bulanan(),
                'c_member_harian'                   => $this->count_member_harian(),
                'c_member_bulanan'                  => $this->count_member_bulanan(),
                'c_pengunjung_harian'               => $this->count_pengunjung_harian(),
                'c_pengunjung_bulanan'              => $this->count_pengunjung_bulanan(),
                'c_pengunjung_gunakan_sistem'       => $this->count_pengunjung_gunakan_sistem(),
                'c_users'                           => $this->count_users(),
                'labelsTamu'                        => "'" . implode("','", $labelsTamu) . "'",
                'dataTamu'                          => implode(",", $dataTamu),
                'labelsMember'                      => "'" . implode("','", $labelsMember) . "'",
                'dataMember'                        => implode(",", $dataMember),
            ];

        echo view('templates/header-dashboard');
        echo view('dashboard', $data);
        echo view('templates/footer');
    }

    public function update($id)
    {
        $model = new TransactionModel();

        $id = $this->request->getPost('id');
        $plat = $this->request->getPost('plat');
        $keperluan = $this->request->getPost('keperluan');
        $data = [
            'idpetugas' => session()->id,
            'plat'      => $plat,
            'keperluan' => $keperluan,
        ];

        $sql = $model->update($id, $data);

        if ($sql) {
            $session = session();

            $template = '$nama_petugas menginput data $plat dengan keperluan $keperluan pada $waktu';
            $vars = array(
                '$nama_petugas' => $session->nama,
                '$waktu' => Time::now(),
                '$plat' => $plat,
                '$keperluan' => $keperluan,
            );

            $logText = strtr($template, $vars);
            $logData = [
                'nama_petugas' => $session->nama,
                'deskripsi' => $logText,
                'created_at' => Time::now(),
            ];

            $logModel = new LogModel();
            $logModel->save($logData);
            session()->setFlashdata('update_success', 'Data berhasil diperbarui.');

            return redirect()->to('/dashboard');
        }
    }

    function count_tamu_harian()
    {
        $date = date('Y-m-d');

        $this->builder->select('*');
        $this->builder->join('masterdata', 'masterdata.rfid = transaksi.rfid_masuk');
        $this->builder->where('masterdata.level', 'Petugas');
        $this->builder->like('transaksi.tglmasuk', $date);
        $sql = $this->builder->countAllResults();

        return $sql;
    }

    function count_tamu_bulanan()
    {
        $date = date('Y-m');

        $this->builder->select('*');
        $this->builder->join('masterdata', 'masterdata.rfid = transaksi.rfid_masuk');
        $this->builder->where('masterdata.level', 'Petugas');
        $this->builder->like('transaksi.tglmasuk', $date);
        $sql = $this->builder->countAllResults();

        return $sql;
    }

    function count_member_harian()
    {
        $date = date('Y-m-d');

        $this->builder->select('*');
        $this->builder->join('masterdata', 'masterdata.rfid = transaksi.rfid_masuk');
        $this->builder->where('masterdata.level', 'Member');
        $this->builder->like('transaksi.tglmasuk', $date);
        $sql = $this->builder->countAllResults();

        return $sql;
    }

    function count_member_bulanan()
    {
        $date = date('Y-m');

        $this->builder->select('*');
        $this->builder->join('masterdata', 'masterdata.rfid = transaksi.rfid_masuk');
        $this->builder->where('masterdata.level', 'Member');
        $this->builder->like('transaksi.tglmasuk', $date);
        $sql = $this->builder->countAllResults();

        return $sql;
    }

    function count_pengunjung_harian()
    {
        $date = date('Y-m-d');

        $this->builder->select('*');
        $this->builder->join('masterdata', 'masterdata.rfid = transaksi.rfid_masuk');
        $this->builder->whereIn('masterdata.level', ['Member', 'Petugas']);
        $this->builder->like('transaksi.tglmasuk', $date);
        $sql = $this->builder->countAllResults();

        return $sql;
    }

    function count_pengunjung_bulanan()
    {
        $date = date('Y-m');

        $this->builder->select('*');
        $this->builder->join('masterdata', 'masterdata.rfid = transaksi.rfid_masuk');
        $this->builder->whereIn('masterdata.level', ['Member', 'Petugas']);
        $this->builder->like('transaksi.tglmasuk', $date);
        $sql = $this->builder->countAllResults();

        return $sql;
    }

    function count_pengunjung_gunakan_sistem()
    {
        $this->builder->select('*');
        $this->builder->join('masterdata', 'masterdata.rfid = transaksi.rfid_masuk');
        $this->builder->where('masterdata.level', 'Member');
        $sql = $this->builder->countAllResults();

        return $sql;
    }

    function count_users()
    {
        $userModel = new AuthModel();
        $sql = $userModel->countAllResults();

        return $sql;
    }

    function count_tamu_member_tahunan($level)
    {
        $now = Time::now();
        $parse = Time::parse($now, 'Asia/Jakarta');
        $year = $parse->getYear();

        $this->builder->select('COUNT(transaksi.id) AS jumlah, MONTH(transaksi.tglmasuk) AS bulan');
        $this->builder->join('masterdata', 'masterdata.rfid = transaksi.rfid_masuk');
        $this->builder->where('level', $level);
        $this->builder->where('YEAR(transaksi.tglmasuk)', $year);
        $this->builder->groupBy('bulan');
        $tamu = $this->builder->get();
        return $tamu;
    }

    // function count_member_tahunan()
    // {
    //     $now = Time::now();
    //     $parse = Time::parse($now, 'Asia/Jakarta');
    //     $year = $parse->getYear();

    //     $this->builder->select('COUNT(transaksi.id) AS jumlah, MONTH(transaksi.tglmasuk) AS bulan');
    //     $this->builder->join('masterdata', 'masterdata.rfid = transaksi.rfid_masuk');
    //     $this->builder->where('level', 'Member');
    //     $this->builder->where('YEAR(transaksi.tglmasuk)', $year);
    //     $this->builder->groupBy('bulan');
    //     $tamu = $this->builder->get();
    //     return $tamu;
    // }
}
