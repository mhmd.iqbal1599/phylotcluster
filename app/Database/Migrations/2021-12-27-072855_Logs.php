<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Logs extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type'              => 'int',
                'constraint'        => 11,
                'unsigned'          => true,
                'auto_increment'    => true,
            ],
            'nama_petugas' => [
                'type'              => 'varchar',
                'constraint'        => 30,
            ],
            'deskripsi' => [
                'type'              => 'varchar',
                'constraint'        => 255,
            ],
            'created_at DATETIME DEFAULT CURRENT_TIMESTAMP',
        ]);

        $this->forge->addKey('id', TRUE);
        $this->forge->createTable('logs', TRUE);
    }

    public function down()
    {
        $this->forge->dropTable('logs');
    }
}
