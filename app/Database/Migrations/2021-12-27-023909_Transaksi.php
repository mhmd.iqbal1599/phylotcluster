<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Transaksi extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type'              => 'int',
                'constraint'        => 11,
                'unsigned'          => true,
                'auto_increment'    => true,
            ],
            'rfid_masuk' => [
                'type'              => 'varchar',
                'constraint'        => 16,
            ],

            'tglmasuk DATETIME DEFAULT CURRENT_TIMESTAMP',

            'fotomasuk' => [
                'type'              => 'varchar',
                'constraint'        => 255,
            ],

            'idpetugas' => [
                'type'              => 'int',
                'constraint'        => 11,
                'null'              => true,
            ],
            'keperluan' => [
                'type'              => 'varchar',
                'constraint'        => 255,
                'null'              => true,
            ],
            'plat' => [
                'type'              => 'varchar',
                'constraint'        => 13,
                'null'              => true,
            ],
        ]);

        $this->forge->addKey('id', TRUE);
        $this->forge->createTable('transaksi', TRUE);
    }

    public function down()
    {
        $this->forge->dropTable('transaksi');
    }
}
