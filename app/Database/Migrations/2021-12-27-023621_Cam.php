<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Cam extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type'              => 'int',
                'constraint'        => 11,
                'unsigned'          => true,
                'auto_increment'    => true,
            ],
            'nama_camera' => [
                'type'              => 'varchar',
                'constraint'        => 30,
            ],
            'url_stream' => [
                'type'              => 'varchar',
                'constraint'        => 255,
            ],
        ]);

        $this->forge->addKey('id', TRUE);
        $this->forge->createTable('cam_stream', TRUE);
    }

    public function down()
    {
        $this->forge->dropTable('cam_stream');
    }
}
