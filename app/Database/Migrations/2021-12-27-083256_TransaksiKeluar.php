<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class TransaksiKeluar extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type'              => 'int',
                'constraint'        => 11,
                'unsigned'          => true,
                'auto_increment'    => true,
            ],

            'rfid_keluar' => [
                'type'              => 'varchar',
                'constraint'        => 16,
                'null'              => true,
            ],

            'tglkeluar DATETIME DEFAULT CURRENT_TIMESTAMP',

            'fotokeluar' => [
                'type'              => 'varchar',
                'constraint'        => 255,
                'null'              => true,
            ],

            'created_at DATETIME DEFAULT NULL',
            'updated_at DATETIME DEFAULT NULL',
        ]);

        $this->forge->addKey('id', TRUE);
        $this->forge->createTable('transaksi_keluar', TRUE);
    }

    public function down()
    {
        $this->forge->dropTable('transaksi_keluar');
    }
}
