<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Masterdata extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type'              => 'int',
                'constraint'        => 11,
                'unsigned'          => true,
                'auto_increment'    => true,
            ],
            'username' => [
                'type'              => 'varchar',
                'constraint'        => 30,
            ],
            'password' => [
                'type'              => 'varchar',
                'constraint'        => 255,
            ],
            'nama' => [
                'type'              => 'varchar',
                'constraint'        => 100,
            ],
            'rfid' => [
                'type'              => 'varchar',
                'constraint'        => 16,
            ],
            'level' => [
                'type'              => 'enum',
                'constraint'        => ['Owner', 'Petugas', 'Member'],
            ],
            'alamat' => [
                'type'              => 'varchar',
                'constraint'        => 255,
            ],
            'plat' => [
                'type'              => 'varchar',
                'constraint'        => 15,
            ],
            'nohp' => [
                'type'              => 'varchar',
                'constraint'        => 15,
            ],
            'tgldaftar DATETIME DEFAULT CURRENT_TIMESTAMP',
            'status' => [
                'type'              => 'enum',
                'constraint'        => ['Aktif', 'Non Aktif'],
            ]
        ]);

        $this->forge->addKey('id', TRUE);
        $this->forge->createTable('masterdata', TRUE);
    }

    public function down()
    {
        $this->forge->dropTable('masterdata');
    }
}
