<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Seedcam extends Seeder
{
    public function run()
    {
        $data = [
            [
                'nama_camera'    => 'Gate Masuk',
                'url_stream'     => 'tes',
            ],
            [
                'nama_camera'    => 'Gate Keluar',
                'url_stream'     => 'tes',
            ],
        ];

        $this->db->table('cam_stream')->emptyTable();

        foreach ($data as $d) {
            $this->db->table('cam_stream')->insert($d);
        }
    }
}
