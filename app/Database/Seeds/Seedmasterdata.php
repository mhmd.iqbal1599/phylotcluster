<?php

namespace App\Database\Seeds;

use App\Libraries\Hash;
use CodeIgniter\Database\Seeder;
use CodeIgniter\I18n\Time;

class Seedmasterdata extends Seeder
{
    public function run()
    {
        $data = [
            [
                'username'      => 'admin',
                'password'      => Hash::make('phylot'),
                'nama'      => 'Admin',
                'rfid'      => '1234567890123456',
                'level'      => 'Owner',
                'alamat'      => 'Phylot Office',
                'plat'      => 'BG 1234 ABC',
                'nohp'      => '081234567890',
                'tgldaftar'      => Time::now(),
                'status'      => 'Aktif',
            ],
            [
                'username'      => 'petugas1',
                'password'      => Hash::make('123123'),
                'nama'      => 'Petugas 1',
                'rfid'      => '1234123412341234',
                'level'      => 'Petugas',
                'alamat'      => 'Phylot Office',
                'plat'      => 'BG 2345 AAA',
                'nohp'      => '081234567890',
                'tgldaftar'      => Time::now(),
                'status'      => 'Aktif',
            ],
            [
                'username'      => 'member1',
                'password'      => Hash::make('123123'),
                'nama'      => 'Member 1',
                'rfid'      => '1000100010001000',
                'level'      => 'Member',
                'alamat'      => 'Phylot Residence',
                'plat'      => 'BG 5678 BBB',
                'nohp'      => '081234567890',
                'tgldaftar'      => Time::now(),
                'status'      => 'Aktif',
            ],
        ];

        $this->db->table('masterdata')->emptyTable();

        foreach ($data as $d) {
            $this->db->table('masterdata')->insert($d);
        }
    }
}
