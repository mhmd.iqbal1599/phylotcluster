<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\I18n\Time;

class Seedtransaksi extends Seeder
{
    public function run()
    {
        $data = [
            [
                'rfid_masuk'    => '1234123412341234',
                'tglmasuk'      => Time::now(),
                'fotomasuk'     => 'dummy_photo_1.jpg',
                'idpetugas'     => NULL,
                'keperluan'     => NULL,
                'plat'          => NULL,
            ],
            [
                'rfid_masuk'    => '1234123412341234',
                'tglmasuk'      => Time::tomorrow(),
                'fotomasuk'     => 'dummy_photo_2.jpg',
                'idpetugas'     => NULL,
                'keperluan'     => NULL,
                'plat'          => NULL,
            ],
            [
                'rfid_masuk'    => '1000100010001000',
                'tglmasuk'      => Time::now(),
                'fotomasuk'     => 'dummy_photo_3.jpg',
                'idpetugas'     => NULL,
                'keperluan'     => NULL,
                'plat'          => NULL,
            ],

        ];

        $this->db->table('transaksi')->emptyTable();

        foreach ($data as $d) {
            $this->db->table('transaksi')->insert($d);
        }
    }
}
