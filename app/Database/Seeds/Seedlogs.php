<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Seedlogs extends Seeder
{
    public function run()
    {
        $data = [
            [
                'nama_petugas'  => 'Petugas 1',
                'deskripsi'     => 'Login pada 27/12/2021 - 14:32',
                'created_at'    => '2021-12-27 14:32:15'
            ],
            [
                'nama_petugas'  => 'Petugas 1',
                'deskripsi'     => 'Input data transaksi pada 27/12/2021 - 14:33',
                'created_at'    => '2021-12-27 14:33:15'
            ],
            [
                'nama_petugas'  => 'Petugas 1',
                'deskripsi'     => 'Logout pada 27/12/2021 - 14:33',
                'created_at'    => '2021-12-27 14:33:15'
            ],
        ];

        $this->db->table('logs')->emptyTable();

        foreach ($data as $d) {
            $this->db->table('logs')->insert($d);
        }
    }
}
