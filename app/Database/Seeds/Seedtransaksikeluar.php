<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Seedtransaksikeluar extends Seeder
{
    public function run()
    {
        $data = [
            [
                'rfid_keluar'   => '1234123412341234',
                'tglkeluar'     => '2021-12-27 15:40:15',
                'fotokeluar'    => 'dummy_photo_1.jpg',
                'created_at'     => '2021-12-27 15:40:15',
                'updated_at'     => '2021-12-27 15:40:15',
            ],
            [
                'rfid_keluar'   => '1234123412341234',
                'tglkeluar'     => '2021-12-27 14:42:15',
                'fotokeluar'    => 'dummy_photo_2.jpg',
                'created_at'     => '2021-12-27 15:42:15',
                'updated_at'     => '2021-12-27 15:42:15',
            ],
            [
                'rfid_keluar'   => '1234123412341234',
                'tglkeluar'     => '2021-12-27 14:45:15',
                'fotokeluar'    => 'dummy_photo_2.jpg',
                'created_at'     => '2021-12-27 15:45:15',
                'updated_at'     => '2021-12-27 15:45:15',
            ],
        ];

        $this->db->table('transaksi_keluar')->emptyTable();

        foreach ($data as $d) {
            $this->db->table('transaksi_keluar')->insert($d);
        }
    }
}
