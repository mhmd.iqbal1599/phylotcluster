<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Allseed extends Seeder
{
    public function run()
    {
        $this->call('Seedmasterdata');
        $this->call('Seedcam');
        $this->call('Seedtransaksi');
        $this->call('seedtransaksikeluar');
        $this->call('Seedlogs');
    }
}
